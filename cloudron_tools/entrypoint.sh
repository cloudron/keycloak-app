#!/bin/bash
set -eou pipefail

ADD_PARAMS=""

if [[ ! -f /app/data/.initialized ]]; then
  echo "Fresh installation, setting up data directory..."
  if [[ -z "${INIT_KEYCLOAK_USER+x}" ]]; then
    echo "init user disabled"
  else
    export KEYCLOAK_USER=$INIT_KEYCLOAK_USER
    export KEYCLOAK_PASSWORD=$INIT_KEYCLOAK_PASSWORD
  fi
  if [[ -z "${CLOUDRON_LDAP_BIND_DN+x}" ]]; then
    echo "LDAP disabled"
    CLOUDRON_LDAP_ENABLED="false"
  else
    echo "LDAP enabled"
    CLOUDRON_LDAP_ENABLED="true"
  fi
  cat /opt/jboss/cloudtron_config/full-export.json \
    | sed 's/{{LDAP_ENABLED}}/'${CLOUDRON_LDAP_ENABLED:-false}'/g' \
    | sed 's/{{LDAP_BIND_DN}}/'${CLOUDRON_LDAP_BIND_DN:-cn=dummy,ou=apps,dc=cloudron}'/g' \
    | sed 's/{{LDAP_BIND_PASSWORD}}/'${CLOUDRON_LDAP_BIND_PASSWORD:-***}'/g' \
    | sed 's/{{LDAP_GROUPS_BASE_DN}}/'${CLOUDRON_LDAP_GROUPS_BASE_DN:-ou=groups,dc=cloudron}'/g' \
    | sed 's/{{LDAP_PORT}}/'${CLOUDRON_LDAP_PORT:-}'/g' \
    | sed 's/{{LDAP_SERVER}}/'${CLOUDRON_LDAP_SERVER:-}'/g' \
    | sed 's@{{LDAP_URL}}@'${CLOUDRON_LDAP_URL:-http://localhost:8080}'@g' \
    | sed 's/{{LDAP_USERS_BASE_DN}}/'${CLOUDRON_LDAP_USERS_BASE_DN:-ou=users,dc=cloudron}'/g' \
    | sed 's/{{MAIL_SMTPS_PORT}}/'${CLOUDRON_MAIL_SMTPS_PORT:-1234}'/g' \
    | sed 's/{{MAIL_SMTP_PASSWORD}}/'${CLOUDRON_MAIL_SMTP_PASSWORD:-***}'/g' \
    | sed 's/{{MAIL_SMTP_PORT}}/'${CLOUDRON_MAIL_SMTP_PORT:-1234}'/g' \
    | sed 's/{{MAIL_SMTP_SERVER}}/'${CLOUDRON_MAIL_SMTP_SERVER:-10.0.0.1}'/g' \
    | sed 's/{{MAIL_SMTP_USERNAME}}/'${CLOUDRON_MAIL_SMTP_USERNAME:-dummy}'/g' \
    > /run/full-export.json

  ADD_PARAMS="-Dkeycloak.migration.action=import -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.strategy=OVERWRITE_EXISTING -Dkeycloak.migration.file=/run/full-export.json"
  mkdir -p /tmp/keycloak
  cp -R /opt/jboss/keycloak/standalone/log.orig /app/data/log
  cp -R /opt/jboss/keycloak/standalone/deployments.orig /app/data/deployments
  cp -R /opt/jboss/keycloak/standalone/configuration.orig /app/data/configuration
  cp -R /opt/jboss/keycloak/standalone/data.orig /app/data/data
  touch /app/data/.initialized
  echo "Done."
fi

chown -R jboss:jboss /app/data
chown -R jboss:jboss /tmp/keycloak

export DB_VENDOR="postgres"
export DB_ADDR=$CLOUDRON_POSTGRESQL_HOST
export DB_DATABASE=$CLOUDRON_POSTGRESQL_DATABASE
export DB_USER=$CLOUDRON_POSTGRESQL_USERNAME
export DB_PASSWORD=$CLOUDRON_POSTGRESQL_PASSWORD
export DB_PORT=$CLOUDRON_POSTGRESQL_PORT
export PROXY_ADDRESS_FORWARDING=true
#export KEYCLOAK_FRONTEND_URL=$APP_ORIGIN
echo "****************************************"
echo "Vars after:"
set
echo "****************************************"

exec /usr/local/bin/gosu jboss:jboss /opt/jboss/tools/docker-entrypoint.sh "$ADD_PARAMS $@"