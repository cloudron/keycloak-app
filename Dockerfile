﻿FROM registry.gitlab.com/id4me/openid4meprovider_keycloak_backend/keycloak-cloudron:9.0.0

USER root

COPY --from=gosu/assets /opt/gosu /opt/gosu
RUN /opt/gosu/gosu.install.sh && rm -fr /opt/gosu

RUN mkdir -p /app/data && chown -R 1000:1000 /app/data \
    && mkdir -p /tmp/keycloak && chown -R 1000:1000 /tmp/keycloak \
    && mv /opt/jboss/keycloak/standalone/log /opt/jboss/keycloak/standalone/log.orig \
    && mkdir /app/data/log \
    && ln -sf /app/data/log /opt/jboss/keycloak/standalone/log \
    && mv /opt/jboss/keycloak/standalone/deployments /opt/jboss/keycloak/standalone/deployments.orig \
    && mkdir /app/data/deployments \
    && ln -sf /app/data/deployments /opt/jboss/keycloak/standalone/deployments \
    && mv /opt/jboss/keycloak/standalone/configuration /opt/jboss/keycloak/standalone/configuration.orig \
    && ln -sf /app/data/configuration /opt/jboss/keycloak/standalone/configuration \
    && mv /opt/jboss/keycloak/standalone/data /opt/jboss/keycloak/standalone/data.orig \
    && ln -sf /app/data/data /opt/jboss/keycloak/standalone/data \
    && rm -fr /opt/jboss/keycloak/standalone/tmp \
    && ln -sf /tmp/keycloak /opt/jboss/keycloak/standalone/tmp

ADD cloudron_tools /opt/jboss/cloudron_tools

ADD config /opt/jboss/cloudtron_config
ADD themes/address /opt/jboss/keycloak/themes/address

RUN chown -R 1000:1000 /opt/jboss/cloudtron_config \
    && chown -R 1000:1000 /opt/jboss/keycloak/themes/address

ENTRYPOINT [ "/opt/jboss/cloudron_tools/entrypoint.sh" ]