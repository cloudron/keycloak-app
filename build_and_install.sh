#!/bin/bash
DOMAIN=<domain_name>
docker build -t registry.gitlab.com/id4me/openid4meprovider_keycloak_backend/keycloak-cloudron:9.0.0 ./build && docker push registry.gitlab.com/id4me/openid4meprovider_keycloak_backend/keycloak-cloudron:9.0.0
TAG=9.0.0_6 && docker build -t registry.gitlab.com/id4me/openid4meprovider_keycloak_backend:$TAG . && docker push registry.gitlab.com/id4me/openid4meprovider_keycloak_backend:$TAG && cloudron install --image registry.gitlab.com/id4me/openid4meprovider_keycloak_backend:$TAG -l $DOMAIN
cloudron logs -f --app $DOMAIN